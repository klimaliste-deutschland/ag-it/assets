var JSWSelfScriptPath = document.getElementsByTagName('script')[document.getElementsByTagName('script').length-1].src;
var JSWDefaultPaths = { units : ['modules/'], libraries : ['other/'], applications : ['applications/'], icons : '../icons/', designInfo : '../css/designinfo.json' };
function unitLoadDefault () { throw 'No unit manager active in current window!'; }
var unitLoad = unitLoadDefault;

function JSWUnitManager (paths) {
 this.paths = paths;
 // set selfScriptPath
 if (!this.paths.selfScriptPath) {
  this.paths.selfScriptPath = JSWSelfScriptPath;
  if (this.paths.selfScriptPath.indexOf('\/JSWSystem.js') < 0) throw 'JSWSystem.js kann seinen eigenen Pfad nicht feststellen! Die Datei JSWSystem.js darf nicht umbenannt werden und muss beim laden der Seite geladen werden.';
  this.paths.selfScriptPath = this.paths.selfScriptPath.substring(0, this.paths.selfScriptPath.lastIndexOf('\/')+1);
 }
 // make paths absolute
 for (var key in this.paths) {
  if (typeof this.paths[key] == 'string') this.paths[key] = absolutePath(this.paths[key], this.paths.selfScriptPath);
  else for (var i = 0; i < this.paths[key].length; i++) this.paths[key][i] = absolutePath(this.paths[key][i], this.paths.selfScriptPath);
 }
 // rest of init
 if (!this.paths.libraries) this.paths.libraries = this.paths.units;
 this.units = new Object();
 var _this = this;
 if (unitLoad != unitLoadDefault) throw 'There is already an active unit manager in current window!';
 unitLoad = function (uName, units, unitFkt) {
  if (units && units.length > 0) _this.uses(_this.units[uName], units, function () { new unitFkt(_this.units[uName], _this); });
  else new unitFkt(_this.units[uName], _this);
 };
}
JSWUnitManager.prototype = {
 // public
 defaultDesign : 'default',
 // public readonly
 stylesheets : null,
 // protected
 units : null,
 paths : null, // contains: selfScriptPath:Path of this js-file   units:Array of unit search paths    libraries:Array of library search paths    applications:Array of application search paths    designInfo:Path of design info json    icons    
 onDesignLoaded : null,
 onUnitLoaded : null,

 // garantees, that the unit uName is loaded when callback is called. Adds dependency to unit's dependencies. Can also load scripts, that are no units by setting isLib to true. uPaths is an array of one or more possible folders of the unit; tries first folder, if unit isn't located there (404-error) tries second folder, etc.
 loadUnit : /* protected */ function (uPaths, uName, dependency, callback, isLib) { // can be called recoursively!
  if (!uPaths) uPaths = [''];
  var u = this.units[uName];
  if (u && this.units[uName].loaded) { // Unit wurde vollstaendig geladen
   u.dependencies.push(dependency);
   callback(u);
  } else if (u) { // Unit wird bereits geladen
   u.dependencies.push(dependency);
   var oldOnload = u.onload;
   u.onload = function (unit, err) {
    oldOnload(unit, err);
    callback(u);
   };
  } else { // Unit wurde noch nicht geladen
   var _this = this;
   if (isLib) for (var key in this.units) if (this.units[key].isLib && !this.units[key].loaded) { // ensure, to load not more than one library at a time due to inter-library dependencies
    var oldOnload = this.units[key].onload;
    this.units[key].onload = function (unit, err) {
     oldOnload(unit, err);
     _this.loadUnit(uPaths, uName, dependency, callback, isLib);
    };
    return;
   }
   u = new Object();
   this.units[uName] = u;
   u.name = uName;
   u.manager = _this;
   u.isLib = isLib;
   u.dependencies = new Array(dependency); // units/windows, die diese Unit benoetigen
   u.loaded = false;
   u.onload = function (unit, err) {
    if (u.loaded) throw 'Unit "'+u.name+'" is already loaded!';
    u.loaded = true;
    u.err = err;
    if (unit) u.unit = unit;
    if (err) throw err; // no callback on error
    if (_this.onUnitLoaded) _this.onUnitLoaded(u);
    if (callback) callback(u);
   };
   u.forceUnload = function () {
    try { u.unit.free(); } catch (e) { }
    document.getElementsByTagName('head')[0].removeChild(u.script);
    delete _this.units[u.name];
    _this.removeDependencies(u);
    for (var key in u) if (typeof u[key] == 'object') u[key] = null;
    u = null;
   };
   pathNr = 0;
   function testNextLocation () {
    u.script = document.createElement('script');
    if (isLib) u.script.addEventListener('load', function () { u.onload(null); }, false);
    u.script.addEventListener('error', function () {
     document.getElementsByTagName('head')[0].removeChild(u.script);
     u.script = null;
     if (pathNr < uPaths.length-1) {
      pathNr++;
      testNextLocation();
     } else u.onload(null, 'Unit "'+uName+'" not found!'); // call onload even in error-case for not blocking i.e. the loading of further libraries
    }, false);
    u.script.type = 'text/javascript';
    u.script.src = uPaths[pathNr] + uName + '.js';
    document.getElementsByTagName('head')[0].appendChild(u.script);
   }
   this.addOnStart(testNextLocation); // ensure, that units will not load before start of inherited class (i.e. JSWSystem)
  }
 },
 
 // simple function for loading a unit without need of a dependency object. For holding the dependency, a dummy-object is created and returned by this function. If this object isn't stored, the unit cannot be unloaded later. This function can also load a unit by it's full path name.
 dynamicallyLoadUnit : /* public */ function (uLocation, callback) {
  if (!callback) callback = function () { };
  var uPath = uLocation.split('\/');
  var uName = uPath.pop().split('.')[0];
  if (uPath.length == 0) uPath = this.paths.units;
  else uPath = [uPath.join('\/') + '\/'];
  var dependency = new Object();
  this.loadUnit(uPath, uName, dependency, callback);
  return dependency;
 },
 
 checkForApplicationStart : /* protected */ function () { }, // to be overridden
 iterateDocuments : /* protected */ function (callback) { callback(window.document); }, // to be overridden
 addOnStart : /* public */ function (fkt) { fkt(); }, // to be overridden

 // loads one or more units for an object (i.e. window or unit-obj) as dependency and additionally stores it in obj.jswUnits if obj is a window (required in checkForApplicationStart)
 uses : /* public */ function (obj, units, callback) {
  if (typeof units == 'string') units = [units];
  var loaded = 0;
  var lastLib = 0;
  for (var i = 0; i < units.length; i++) {
   // check, if units[i] is a library
   var isLib = false;
   if (units[i].indexOf('lib:') == 0) {
    isLib = true;
    units[i] = units[i].substring(4, units[i].length);
   }
   // store used units in obj (if obj is a window)
   if (obj.self == obj) {
    if (!obj.jswUnits) obj.jswUnits = new Array();
    if (obj.jswUnits.indexOf(units[i]) == -1) obj.jswUnits.push(units[i]);
   }
   // load...
   var _this = this;
   this.loadUnit((isLib)?_this.paths.libraries:_this.paths.units, units[i], obj, function (u) {
    if (obj.self == obj && obj.jswApplication) _this.checkForApplicationStart(obj.jswApplication);
	if (u.onUsed) u.onUsed(obj);
    loaded++;
    if (loaded >= units.length && callback) callback((units.length==1)?u.unit:null);
   }, isLib);
  }
 },
 
 // returns the file name without ending of a JSW-managed style element
 styName : /* public */ function (sty) {
  if (sty.rel != 'stylesheet' || (' '+sty.className+' ').indexOf(' JSW ') == -1) return ''; // not a JSW-managed stylesheet
  if (!sty.jswName) {
   sty.jswName = sty.href.split('\/').pop();
   if (sty.jswName.lastIndexOf('.css') == sty.jswName.length-4) sty.jswName = sty.jswName.substring(0, sty.jswName.length-4);
  }
  return sty.jswName;
 },
 
 applyDesign : /* public */ function (info, infoUrl, design, recursiveCall) {
  if (!recursiveCall) this.stylesheets = new Array();
  var s = info[design];
  if (!s) throw 'Design "'+design+'" not found!';
  if (s.inherits) this.applyDesign(info, infoUrl, s.inherits, true);
  for (key in s.files) this.stylesheets[key] = absolutePath(s.files[key].url, infoUrl);
 },
 
 // call this, when obj requires the stylesheet with name css in document doc
 usesCSS : /* public */ function (obj, doc, css, callback) {
  if (!this.stylesheets) { // no design loaded. Load standard design and try again...
   var _this = this;
   if (!this.onDesignLoaded) {
    this.onDesignLoaded = [function () { _this.usesCSS(obj, doc, css, callback); }];
	// load default design
	jsw.parseJSONFile(this.paths.designInfo, function (json) {
	 _this.applyDesign(json, _this.paths.designInfo, _this.defaultDesign);
	 for (var i = 0; i < _this.onDesignLoaded.length; i++) _this.onDesignLoaded[i]();
	});
   } else this.onDesignLoaded.push(function () { _this.usesCSS(obj, doc, css, callback); });
   return;
  }
  var head = doc.getElementsByTagName('head')[0];
  var stys = head.getElementsByTagName('link');
  var sty = null;
  for (var i = 0; i < stys.length; i++) if (this.styName(stys[i]) == css) { sty = stys[i]; break; }
  if (sty == null) {
   var styleInfo = this.stylesheets[css];
   if (!styleInfo) throw 'Stylesheet "'+css+'" not availlable in current design!';
   sty = doc.createElement('link');
   sty.rel = 'stylesheet';
   sty.className = 'JSW';
   sty.href = styleInfo;
   head.appendChild(sty);
  }
  if (obj) {
   if (!sty.jswDependencies) sty.jswDependencies = new Array();
   if (sty.jswDependencies.indexOf(obj) == -1) sty.jswDependencies.push(obj);
  } else sty.jswDependencies = [null]; // never unload this stylesheet
  if (callback) callback();
 },
 
 // call this function, when the obj is freed and does not longer require any other unit or stylesheet
 removeDependencies : /* private */ function (obj) {
  // remove unnecessary units
  var a = new Array();
  for (var key in this.units) {
   if (!this.units[key]) continue;
   for (var j = this.units[key].dependencies.length - 1; j >= 0; j--) if (this.units[key].dependencies[j] == obj) this.units[key].dependencies.splice(j, 1);
   if (this.units[key].dependencies.length == 0) a.push(this.units[key]);
  }
  for (var i = 0; i < a.length; i++) a[i].forceUnload();
  a = null;
  // remove unneccessary styles
  this.iterateDocuments(function (doc) {
   var head = doc.getElementsByTagName('head')[0];
   var stys = head.getElementsByTagName('link');
   for (var i = stys.length-1; i >= 0; i--) {
    if (!stys[i].jswDependencies) continue;
	var j = stys[i].jswDependencies.indexOf(obj);
	if (j != -1) stys[i].jswDependencies.splice(j, 1);
	if (stys[i].jswDependencies.length == 0) head.removeChild(stys[i]);
   }
  });
 }
}

function JSWUnitManagerPrototype () { }
JSWUnitManagerPrototype.prototype = JSWUnitManager.prototype;




function JSWSystem /* extends JSWUnitManager */ () {
 // inherited Constructor
 JSWUnitManager.call(this, JSWDefaultPaths);
 
 // private
 var onStart = new Array();
 var applications = new Array();
 var standardConsole = new JSWConsole();
 var _this = this;
 
 // protected
 this.selfHostname = '';

 // public (Readonly)
 this.mgr = null;
 this.taskbar = null;
 this.whitelist = new Array();
 this.idWin = new Object();
 this.standardPos = { top : 100, left : 100, width : 400, height : 200, right : Number.NaN, bottom : Number.NaN };
 this.standardBehavoir = 16;
  
 this.start = /* public */ function () {
  // check
  if (_this.mgr) return;
  var el = document.body;
  if (!el) return;
  if (!window.JSWManager) throw 'JSWManager.js must be imported before JSWSystem.js!';
  if (!window.FrameCommunicator) throw 'Zum Erstellen von JSWSystem wird frameCommunicator.js benoetigt!';
  // initialization
  selfHostname = getHostnameAndProtocol(self.location.href);
  _this.mgr = new JSWManager(el);
  _this.taskbar = new JSWTaskbar(_this.mgr);
  if (document.getElementById('menu')) _this.taskbar.setMenu(document.getElementById('menu'));
  autoCreateWindows();
  for (var i = 0; i < onStart.length; i++) onStart[i]();
  onStart = null;
  _this.mgr.addEventListener('hide', onWinHide);
  window.setTimeout(function () { _this.processSite(document.body); }, 1000);
 };

 if (document.addEventListener) document.addEventListener('DOMContentLoaded', _this.start, false);
 
 var onWinHide = /* private */ function (win) { // wird aufgerufen, wenn ein Fenster geschlossen wird. Nach dem Schliessen werden auch die ContentWindows der Iframes des Fensters freigegeben (Auch, wenn das Fenster selbst nicht freigegeben wird)!
  for (var i = 0; i < win.el['InnerBox'].getElementsByTagName('iframe').length; i++) _this.removeDependencies(win.el['InnerBox'].getElementsByTagName('iframe')[i].contentWindow);
 };

 var autoCreateWindows = function () {
  var el = _this.mgr.parentElement.getElementsByTagName('div');
  for (var i = 0; i < el.length; i++) {
   var n = ' '+el[i].className+' ';
   if (n.indexOf(' JSW ') < 0 || n.indexOf(' OuterBox ') < 0) continue;
   (new JSWindowManaged(_this.mgr, null, el[i])).setRecoveryURL((el[i].id)?'$'+el[i].id:'');
  }
 };

 var isNotInWhitelist = /* private */ function (src) {
  if (getHostnameAndProtocol(src) == selfHostname) return false;
  for (var i = 0; i < _this.whitelist.length; i++) if (_this.whitelist[i].test(src)) return false;
  return true;
 };

 var titleFormURL = /* private */ function (url) {
  url = url.split('\/');
  var i = url.length-1;
  while (i > 0 && url[i] == '') i--;
  return url[i];
 };
 
 this.iterateDocuments = /* protected */ function (callback) {
  JSWUnitManager.prototype.iterateDocuments.call(this, callback); // inherited
  this.iterateWindows(function (win) { if (win.mainFrame) try { callback(win.mainFrame.contentWindow.document); } catch (e) {} });
 };
 
 this.iterateWindows = /* public */ function (callback) {
  if (!_this.mgr) return;
  for (var i = 0; i < _this.mgr.layers.length; i++) for (var j = 0; j < _this.mgr.layers[i].windows.length; j++) callback(_this.mgr.layers[i].windows[j]);
 };

 this.processSite = /* public */ function (el, mainWindow) {
  function addListener (el, max, ex, sameLayer) {
   el.addEventListener('click', function (ev) {
    var b = 19;
    if (max) b += 4;
    if (ex) b += 8;
    var l = null;
    if (sameLayer && mainWindow) l = mainWindow.layer;
    var w = jsw.createWindowFromSite(this.href, b, l);
    if (ev.preventDefault) ev.preventDefault();
    return false;
   }, false);
   el.jswProcessed = true;
  }
  var a = el.getElementsByTagName('a');
  for (var i = 0; i < a.length; i++) if ((' '+a[i].className+' ').indexOf(' WindowLink ') != -1 && !a[i].jswProcessed) addListener(a[i], ((' '+a[i].className+' ').indexOf(' openMaximized ') != -1), ((' '+a[i].className+' ').indexOf(' openExcludable ') != -1), ((' '+a[i].className+' ').indexOf(' openInSameLayer ') != -1));
 };

 this.addOnStart = /* public */ function (fkt) {
  if (_this.mgr) fkt();
  else onStart.push(fkt);
 };

 this.addURLToWhitelist = /* private */ function (url) {
  var rep = new RegExp('([^\\w&^\\*])', 'g');
  var host = getHostnameAndProtocol(url);
  var i = host.length;
  host = host.replace(rep, '\\$1');
  var path = url.substring(i, url.length).replace(rep, '\\$1');
  if (path == '' && host.length > 0 && host.charAt(host.length-1) == '*') path = '*';
  while (host.length > 0 && host.charAt(host.length-1) == '*') host = host.substring(0, host.length-1);
  if (host == '') throw 'URL has wrong syntax';
  rep = new RegExp('\\*', 'g');
  path = path.replace(rep, '[a|^a]*'); // innerhalb des path-Bereiches koennen die * fuer beliebige Inhalte stehen
  // innerhalb des hostname-Bereiches haben die * spezielle Aufgaben
  var a = host.split('\\\:\\\/\\\/');
  if (a.length == 1) {
   a = a[0].split('*');
   if (a.length == 2) {
    host = a[0] + '(\\w)*\\:\\/\\/((\\w)+\\.)*' + a[1];
   } else throw 'URL has wrong syntax';
  } else if (a.length == 2) {
   rep = new RegExp('(\\*)', 'g');
   a[0] = a[0].replace(rep, '(\\w)*');
   a[1] = a[1].replace(rep, '((\\w)+\\.)*');
   host = a[0] + '\\:\\/\\/' + a[1];
  } else throw 'URL has wrong syntax';
  _this.whitelist.push(new RegExp('\^'+host+path));
 };
 
 this.checkForApplicationStart = /* protected */ function (app) {
  if (app.started || !app.loaded || app.terminated) return;
  if (app.win.jswUnits) for (var i = 0; i < app.win.jswUnits.length; i++) if (!_this.units[app.win.jswUnits[i]].loaded) return;
  if (!app.win.main) throw app.frame.src + ' is not a valid jsw-application!';
  app.started = true;
  app.win.main(app.outerBox);
 };
 
 this.runApplication = /* public */ function (url, params, console, callback, launchedFromAdressbar) {
  if (!console) console = standardConsole;
  // makeup url
  if (url.lastIndexOf('.html') != url.length-5) url += '.html';
  // ensure params is an array
  if (typeof params == 'string') params = [params];
  if (!params) params = new Array();
  // prepare application object
  var a = new Object();
  applications.push(a);
  a.loaded = false;
  a.started = false;
  a.terminated = false;
  a.console = console;
  a.params = params;
  a.onConsoleEnter = null;  
  // application functions
  a.createMainWindow = function (pos) {
   if (a == null) throw 'Application terminated!';
   var w = new JSWindowManaged(_this.mgr, null, a.outerBox);
   w.freeOnHide = true;
   a.mainWindow = w;
   w.mainFrame = a.frame;
   a.communicator = new FrameCommunicator(a.frame);
   w.pos = (pos)?pos:clone(_this.standardPos);
   w.setPos();
   w.title = a.communicator.contentTitle;
   w.addEventListener('free', function (w) { // call application.terminate if main window gets freed
    if (a == null || a.terminated) return;
    a.mainWindow = null;
    a.terminate();
   });
   w.show();
  }
  a.terminate = function () {
   if (a == null) throw 'Application terminated!';
   if (a.win) {
    if (a.win.terminate) a.win.terminate();
    _this.removeDependencies(a.win);
	a.win.jswApplication = null;
   }
   if (a.console && a.console.listeningApps) a.console.listeningApps.splice(a.console.listeningApps.indexOf(a), 1);
   a.terminated = true;
   a.frame.src = '';
   if (a.mainWindow) a.mainWindow.free();
   else try { document.body.removeChild(a.outerBox); } catch (e) {}
   for (var key in a) if (typeof a == 'object') a[key] = null;
   a = null;
   if (callback) callback();
  }
  // inner- und Outer box, falls Anwendung ein MainWindow erzeugen möchte
  a.outerBox = document.createElement('div');
  a.outerBox.className = 'JSW OuterBox';
  a.outerBox.style.visibility = 'hidden';
  document.body.appendChild(a.outerBox);
  a.innerBox = document.createElement('div');
  a.innerBox.className = 'JSW InnerBox';
  a.outerBox.appendChild(a.innerBox);
  // create application frame
  a.frame = document.createElement('iframe');
  a.frame.addEventListener('load', function () {
   a.loaded = true;
   a.win = a.frame.contentWindow;
   if (launchedFromAdressbar && !a.win.enableAdressBarLaunch) throw 'Application does not allow launch from adressbar!';
   a.win.jswApplication = a;
   _this.checkForApplicationStart(a);
  });
  a.frame.style.marginBottom = '-4px';
  a.frame.style.width = '100%';
  a.frame.style.height = '100%';
  a.frame.style.border = '0px none transparent';
  // append application to console
  if (a.console && a.console.listeningApps) a.console.listeningApps.push(a);
  // run application frame
  a.innerBox.appendChild(a.frame);
  var pathNr = 0;
  function testNextLocation () {
   if (pathNr >= _this.paths.applications.length) throw 'Application "'+url+'" not found!';
   var path = absolutePath(url, _this.paths.applications[pathNr]);
   if (path.indexOf(getHostnameAndProtocol(window.location.href)) != 0) throw 'Applications must be of the same domain!';
   pathNr++;
   a.frame.src = path;
  }
  a.frame.addEventListener('error', testNextLocation, false);
  testNextLocation();
  // delete unnecessary function parameters
  params = null;
  console = null;
 };
 
 this.findCorrespondingWindow = /* public */ function (el) {
  var p = el;
  while (p) {
   if (p.jswLinkedWindow) return p.jswLinkedWindow;
   p = p.parentElement;
  }
  p = el.ownerDocument.defaultView;
  if (p.jswApplication) return p.jswApplication.mainWindow;
  else return p.jswMainWindow;
 };
 
 this.createWindowFromSite = /* public */ function (url, behavoir, layer) {
  if (!behavoir) behavoir = _this.standardBehavoir;
  _this.start();
  url = absolutePath(url);
  if (isNotInWhitelist(url)) return;
  // window erstellen
  var w = new JSWindowManaged(_this.mgr, layer, null);
  if (w.freed) return null; // Fenster hat sich selbst freigegeben
  w.pos = clone(_this.standardPos);
  w.title = titleFormURL(url);
  // iframe-Element erstellen
  var f = document.createElement('iframe');
  f.style.marginBottom = '-4px';
  f.style.width = '100%';
  f.style.height = '100%';
  f.style.border = '0px none transparent';
  w.el['InnerBox'].appendChild(f);
  // communicator erstellen
  var c = new FrameCommunicator(f);
  c.onTitleChanged = function (title) { if (title != '') w.title = title; };
  c.onURLChanged = function (host, url) { w.setRecoveryURL(url); };
  w.mainFrame = f;
  // Seite laden
  w.showFrameload(_this.mgr.parentElement);
  var load = function () {
   if (w.freed) return null; // Fenster hat sich selbst freigegeben
   if (behavoir & 4) w.maximize();
   if (behavoir & 8) w.excludable = true;
   if (behavoir & 16) w.titleToID();
   if (w.freed) return null; // Fenster hat sich selbst freigegeben
   w.show();
   if (behavoir & 1) w.activate();
   if (behavoir & 2) w.bringToFront();
   if (behavoir & 32) w.exclude();
   if (behavoir & 64) w.minimize();
   if (behavoir & 128) JSWEffect_terminate(w);
  }
  if (document.addEventListener) f.addEventListener('load', load, false); else f.attachEvent('onload', load);
  f.src = url;
  return w;
 };
 
 this.parseJSON = function (str, callback) {
  var blob = new Blob(["addEventListener('message', function (ev) { var json; eval('json='+ev.data); postMessage(json); }, false);"]);
  var worker = new Worker(URL.createObjectURL(blob));
  worker.addEventListener('message', function (ev) {
   worker.terminate();
   callback(ev.data);
  }, false);
  worker.postMessage(str);
 };
 
 this.parseJSONFile = function (src, callback) {
  var loader = new XMLHttpRequest();
  loader.overrideMimeType("application/json");
  loader.open('GET', absolutePath(src), true);
  var _this = this;
  loader.onreadystatechange = function () {
   if (loader.readyState == 4) _this.parseJSON(loader.responseText, callback);
  };
  loader.send(null);  
 };

 _this.start(); 
}

JSWSystem.prototype = new JSWUnitManagerPrototype();



JSWConsole = function () {
};

JSWConsole.prototype = {
 log : function (text) { }
};

function JSWConsolePrototype () { }
JSWConsolePrototype.prototype = JSWConsole.prototype;



// initialization
window.jswMainWindow = null;
window.jsw = (function () {
 if (document.getElementsByTagName('script')[document.getElementsByTagName('script').length-1].getAttribute('noautostart') != null) return null;
 else if (parent != self && parent.jsw) {
  for (var i = 0; i < parent.document.getElementsByTagName('iframe').length; i++) {
   if (parent.document.getElementsByTagName('iframe')[i].contentWindow == window) {
    var el = parent.document.getElementsByTagName('iframe')[i];
    while (el && !el.jswLinkedWindow) el = el.parentElement;
    if (el) jswMainWindow = el.jswLinkedWindow;
   }
  }
  if (document.body) parent.jsw.processSite(document.body, jswMainWindow);
  else document.addEventListener('DOMContentLoaded', function () { parent.jsw.processSite(document.body, jswMainWindow); }, false);
  return parent.jsw;
 } else return new JSWSystem();
})();
