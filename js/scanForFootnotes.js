document.addEventListener("DOMContentLoaded", function () {
  var footnotes = new Array();

  function makeLinksKlickable (el) {
    if (el.nodeType == 3) {
      let links = new Array();
      let spl = el.textContent.split(' ');
      for (let i = 0; i < spl.length; i++)
        if (spl[i].indexOf('http:\/\/') == 0 || spl[i].indexOf('https:\/\/') == 0)
          links.push(i);
      if (links.length > 0) {
        let pos = 0;
        let tn;
        for (let i = 0; i < links.length; i++) {
          tn = document.createTextNode('');
          while (pos < links[i]) {
            tn.textContent += spl[pos]+' ';
            pos++;
          }
          if (tn.textContent != '')
            el.parentElement.insertBefore(tn, el);
          let a = document.createElement('a');
          a.href = spl[pos];
          a.target = '_blank';
          a.textContent = spl[pos];
          el.parentElement.insertBefore(a, el);
          pos++;
        }
        tn = document.createTextNode('');
        while (pos < spl.length) {
          tn.textContent += spl[pos];
          if (pos < spl.length-1) tn.textContent += ' ';
          pos++;
        }
        if (tn.textContent != '')
          el.parentElement.insertBefore(tn, el);
        el.parentElement.removeChild(el);
      }
    } else if (el.nodeType == 1 && el.tagName.toLowerCase() != 'a')
      for (let i = el.childNodes.length-1; i >= 0; i--)
        makeLinksKlickable(el.childNodes[i]);
  }

  function createFootnoteRef (note, firstref) {
    let el = document.createElement('sup');
    el.className = 'footnotelink';
    var link = document.createElement('a');
    link.href = '#footnote'+note.notenumber;
    link.addEventListener('click', function () {
      for (let j = 0; j < footnotes.length; j++)
        footnotes[j].className = 'footnote';
     note.className = 'footnote active';
    });
    link.innerHTML = note.notenumber;
    el.appendChild(link);
    if (firstref)
      el.id = 'footnotelink'+note.notenumber;
    return el;
  }

  let a;
  let namednotes = new Object();
  let noterefs = new Array();
  for (let repeat = 0; repeat < 2; repeat++) {
    if (repeat == 0) a = document.getElementsByTagName('span');
    else a = document.getElementsByTagName('footnote');
    for (let i = 0; i < a.length; i++) {
      if ((repeat == 0 && (' '+a[i].className+' ').indexOf(' footnote ') == -1) || a[i].processed) continue;
      a[i].processed = true;
      if (a[i].getAttribute('notename')) namednotes[a[i].getAttribute('notename')] = a[i];
      if (a[i].getAttribute('noteref')) {
        noterefs.push(a[i]);
        continue;
      }
      a[i].notenumber = footnotes.length+1;
      makeLinksKlickable(a[i]);
      a[i].parentElement.insertBefore(createFootnoteRef(a[i], true), a[i]);
      let el = document.createElement('a');
      el.className = 'number';
      el.href = '#footnotelink'+(footnotes.length + 1);
      el.id = 'footnote'+(footnotes.length + 1);
      el.innerHTML = '['+(footnotes.length + 1)+']';
      a[i].insertBefore(el, a[i].firstChild);
      footnotes.push(a[i]);
    }
  }
  if (footnotes.length == 0) return;

  for (let i = 0; i < noterefs.length; i++) {
    let target = namednotes[noterefs[i].getAttribute('noteref')];
    if (target)
      noterefs[i].parentElement.insertBefore(createFootnoteRef(target, false), noterefs[i]);
    noterefs[i].parentElement.removeChild(noterefs[i]);
  }
  
  let el2 = document.getElementById('footer');
  let b = false;
  if (!el2) {
    b = true;
    document.createElement('div');
  }
  el2.className = 'footer';
  el2.id = 'footer';
  el = document.createElement('hr');
  el2.appendChild(el);
  for (let i = 0; i < footnotes.length; i++)
    el2.appendChild(footnotes[i]);
  if (b) document.body.appendChild(el2);
}, true);
